import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  public inputBox;

  @Output()
  public buttonClickEvent: EventEmitter<String> = new EventEmitter<String>();

  sendEvent(){
    this.buttonClickEvent.emit(this.inputBox);
  }
}
